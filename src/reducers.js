import { combineReducers } from 'redux';
import {
  ADD_TAG, REMOVE_TAG, TOOGLE_ITEMS_MENU
} from './actions';
import { events} from './data';


const initialState = {
  activeTags: ['seminars_workshops_training'],
  posts: events,
  itemsMenu: false
};

export default function filterApp(state = initialState, action) {
  if(action.type === ADD_TAG){
    return {
      ...state,
      activeTags: [...state.activeTags, action.tag],
      posts: state.posts.filter((item) => {
        return 1;
      })  
    };
  }
  if (action.type === REMOVE_TAG) {
    return {
      ...state,
      activeTags: state.activeTags.filter((item) => item !== action.tag)
    };
  }
  if (action.type === TOOGLE_ITEMS_MENU) {
    return{
      ...state,
      itemsMenu: !state.itemsMenu
    };
  }
  return state;
}