export const ADD_TAG = 'ADD_TAG';
export const REMOVE_TAG = 'REMOVE_TAG';
export const TOOGLE_ITEMS_MENU = 'TOOGLE_ITEMS_MENU';

export function addTag(tagName) {
  return {
    type: ADD_TAG,
    tag: tagName
  }
}
export function removeTag(tagName) {
  return {
    type: REMOVE_TAG,
    tag: tagName
  }
}
export function toogleTypeItemsMenu(){
  return {
    type: TOOGLE_ITEMS_MENU
  }
}