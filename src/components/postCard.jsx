import React from 'react';
import { render } from 'react-dom';
import { typeToTitle } from "../data";

const PostCard = ({ id, tag, title, location, image, start_date, end_date}) => {


  return (
    <div className="card-item" tag={tag}>
      <p className="category">{typeToTitle[tag]}</p>
      <p className="caption">{title}</p>
      <div className="img-wrap">
        <p className="address">{location}</p>
        <p className="date">{start_date} – {end_date}</p>
        <img src={image} />
			</div>
    </div>
  ); 
}

export default PostCard;