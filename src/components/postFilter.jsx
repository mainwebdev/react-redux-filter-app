import React from "react";
import { render } from "react-dom";
import { typeToTitle } from "../data";

const PostFilter = ({ activeTags, onAddTag, onRemoveTag, onToggleMenu, itemsMenu }) => {
  function addTag(key) {
    onAddTag(key);
  }
  function removeTag(key) {
    onRemoveTag(key);
  }
  function toogleTag(key) {
    if (activeTags.includes(key)) {
      onRemoveTag(key);
    } else onAddTag(key);
  }
  function removeAllTags(){
    Object.keys(typeToTitle).map(key => onRemoveTag(key));
  }
  function toggleMenu(){
    onToggleMenu();
  }
  return (
    <div className="post-filter">
      <header className="header">
        <div className="select-button select-button1" onClick={toggleMenu.bind(this)}>Тип события</div>
        <div className="select-button select-button2" onClick={toggleMenu.bind(this)}>Фильтры</div>
        <div className={itemsMenu ? "select active-select" : "select"}> 
          {Object.keys(typeToTitle).map(key => (
            <div className="select-item" key={key} onClick={toogleTag.bind(this, key)}>
              {typeToTitle[key]}
            </div>
          ))}
        </div>
      </header>
      <div className="tags">
        
          {activeTags.map(activeTagName => (
          <div className="tag-item" key={activeTagName}>
              {typeToTitle[activeTagName]}
              <div className="close" onClick={removeTag.bind(this, activeTagName)}></div>
            </div>

          ))}
        
        <div className="clear" onClick={removeAllTags.bind(this)}>Oчистить</div>
      </div>
    </div>
  );
};
export default PostFilter;
