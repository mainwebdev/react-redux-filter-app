import React from "react";
import { render } from "react-dom";
import PostCard from "./postCard";

const PostCards = ({posts, activeTags}) => {

 const postsList = posts.map((post) => {
   if (activeTags.includes(post.type)) return <PostCard key={post.id} title={post.title} location={post.location} image={post.image_url} start_date={post.start_date} end_date={post.end_date} tag={post.type} />
    }
  );
  return(
    <div className="cards">
      {postsList}
    </div>
  );
  
}


export default PostCards;