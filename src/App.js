import React, { Component, PropTypes } from 'react';
import { bindActionCreators } from 'redux';
import { connect, dispatch } from 'react-redux'; 
import { toogleTypeItemsMenu, addTag, removeTag} from './actions';
import PostCards from './components/postCards';
import PostFilter from './components/postFilter';



const App = ({ posts, activeTags, itemsMenu, onAddTag, onRemoveTag, onToggleMenu}) => {
  return (
    <div className="wrapper">
      <PostFilter activeTags={activeTags} onAddTag={onAddTag} onRemoveTag={onRemoveTag} onToggleMenu={onToggleMenu} itemsMenu={itemsMenu} />
      <PostCards activeTags={activeTags} posts={posts}  />
    </div>
  );
} 

export default connect(
  state => ({
    posts: state.posts,
    activeTags: state.activeTags,
    itemsMenu: state.itemsMenu    
  }),
  dispatch => ({
    onAddTag: (tagName) => dispatch(addTag(tagName)),
    onRemoveTag: (tagName) => dispatch(removeTag(tagName)),
    onToggleMenu: () => dispatch(toogleTypeItemsMenu())
  })
)(App);